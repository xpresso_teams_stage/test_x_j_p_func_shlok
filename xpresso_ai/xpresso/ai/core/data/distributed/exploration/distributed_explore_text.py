"""Exploration on text attribute"""
__all__ = ["DistributedExploreText"]
__author__ = ["Sanyog Vyawahare"]

from pyspark.sql.functions import split, regexp_replace, col, array_remove
from pyspark.ml.feature import NGram, StopWordsRemover
from pyspark.ml import Pipeline
import databricks.koalas as ks
from xpresso.ai.core.logging.xpr_log import XprLogger
from xpresso.ai.core.commons.utils.constants import N_GRAM_VALUE, UNIGRAM, BIGRAM, TRIGRAM\
    , STOPWORD, EMPTY_STRING, SPACE_STRING


# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class DistributedExploreText():
    """Class for numeric analysis"""

    def __init__(self, data):
        self.name = data.name
        self.data = ks.DataFrame(data)
        return

    def build_ngrams(self):
        """
        Pipeline for calculating N-grams
        :return: pipeline
        """
        ngrams = [
            NGram(n=i, inputCol=self.name, outputCol="grams_{0}".format(i))
            for i in range(1, N_GRAM_VALUE + 1)
        ]
        return Pipeline(stages=ngrams)

    @staticmethod
    def calculate_freq_count_n_gram(data, column, top_ngrams):
        """
        Calculates frequency count of N_gram

        :param data: Data to calculate frequency count
        :param column: Input column
        :param top_ngrams: Picks top n N-gram frequency_count
        :return: Dict with N_gram frequency count
        """
        freq_count = data.rdd.flatMap(lambda a: [(w, 1) for w in a[column]]).reduceByKey(
            lambda x, y: x + y).sortBy(lambda x: x[1], ascending=False).take(top_ngrams)

        return dict(freq_count)

    def populate_text(self, remove_stopwords=True, top_ngrams=50):
        """
            Creates a unigram, Bigram and Trigram Distribution of the
            Attribute data.
        """
        regex = "[^0-9A-Za-z]"
        no_na = self.data.dropna()
        no_na = no_na._sdf.withColumn(self.name, regexp_replace(
            col(self.name), regex, SPACE_STRING))
        temp_kdf = no_na.withColumn(self.name, split(self.name, SPACE_STRING))
        temp_kdf = temp_kdf.select(array_remove(col(self.name), EMPTY_STRING).alias(self.name))
        if remove_stopwords:
            remover = StopWordsRemover(inputCol=self.name, outputCol=STOPWORD)
            temp_kdf = remover.transform(temp_kdf).drop(self.name).withColumnRenamed(
                STOPWORD, self.name)

        temp_kdf = self.build_ngrams().fit(temp_kdf).transform(temp_kdf)

        resp = dict()
        resp["unigram"] = self.calculate_freq_count_n_gram(
            data=temp_kdf, column=UNIGRAM, top_ngrams=top_ngrams)

        resp["bigram"] = self.calculate_freq_count_n_gram(
            data=temp_kdf, column=BIGRAM, top_ngrams=top_ngrams)

        resp["trigram"] = self.calculate_freq_count_n_gram(
            data=temp_kdf, column=TRIGRAM, top_ngrams=top_ngrams)

        for ngram in resp.keys():
            resp[ngram] = {tuple(key.split(' ')): val for key, val in resp[ngram].items()}

        return resp
