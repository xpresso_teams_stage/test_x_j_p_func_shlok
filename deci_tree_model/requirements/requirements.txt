pandas==0.25.3
numpy==1.19.3
matplotlib==3.0.2
scikit_learn==0.23.2
